import gulp from'gulp';
import nunjucksRender from 'gulp-nunjucks-render';
import config from '../gulp.config';

gulp.task('template', () => {
	return gulp.src(config.src.templates + '*.html')
		.pipe(nunjucksRender({
			path: [config.src.templates]
		}))
		.pipe(gulp.dest(config.dist.templates));
});
