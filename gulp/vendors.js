import gulp from 'gulp';
import directorySync  from 'gulp-directory-sync';
import config from '../gulp.config';

gulp.task('vendors', () => {
	return gulp.src('').pipe(directorySync(config.src.vendors, config.dist.vendors, {printSummary: true}));
});
