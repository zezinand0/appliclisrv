let srcPath = 'src/',
	distPath = 'dist/',
	config = {
	distPath: distPath,
	srcPath: srcPath,
	src: {
		javascripts: srcPath + 'js/',
		stylesheets: srcPath + 'scss/',
		images: srcPath + 'img/',
		vendors: srcPath + 'vendors/',
		templates: srcPath + './'
	},
	dist: {
		javascripts: distPath + 'js/',
		stylesheets: distPath + 'css/',
		images: distPath + 'img/',
		vendors: distPath + 'vendors/',
		templates: distPath + './'
	},
	COMPATIBILITY: [
		'last 2 versions',
		'ie >= 9',
		'and_chr >= 2.3'
	],
	gulphelp: {
		options: {
			"hideEmpty": true,
			"hideDepsMessage": true,
			"description": ''
		}
	}
};

export default config;
