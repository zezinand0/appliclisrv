export default class Server {

	/**
	 * Constructor of the Server
	 */
	constructor() {
		this.genTable = $('#gen-table > tbody:last-child');
		this.reqTable = $('#req-table > tbody:last-child');
		this.getMeasures();
	}

	getMeasures() {
		$.ajax({
			type: 'POST',
			url: 'server',
			data: {
				'key': 'xdjosia',
			},
			success: (response) => {
				this.genTable.append(response);
				this.reqTable.append(response);
			},
			error: (response) => {
				console.log(response);
			}
		});
	}

	store(phenom, value, genTime, overwrite, nbOverwrite) {
		$.ajax({
			type: 'POST',
			url: 'server/insert',
			data: {
				'phenom': phenom,
				'value': value,
				'genTime': genTime,
				'overwrite': overwrite,
				'nbOverwrite': nbOverwrite
			},
			success: (response) => {
				this.genTable.append(response);
				this.reqTable.append(response);
			},
			error: (response) => {
				console.log(response);
			}
		});
	}
}
