import Server from "./Server";

export default class Generator {

	/**
	 * Constructor of the Generator
	 */
	constructor() {
		this.initTime = 0;
		this.sinLoop = null;
		this.pulseLoop = null;
		this.srv = new Server();
	}

	/**
	 * Méthode pour attribuer une valeur à l'attribut "initTime"
	 * @param value
	 */
	setInitTime(value) {
		this.initTime = value;
	}

	/**
	 * Génère et enregistre une valeur "sin" sur le serveur.
	 * @param phenom le nom du phénomène
	 * @param deltaT l'écart de temps entre chaque génération
	 * @param amp l'amplitude
	 * @param offset le décallage
	 * @param difference l'écart
	 * @param overwrite true s'il faut écraser des enregistrement
	 * @param nbOverwrite nb d'enregistrement à écraser (0 si overwrite est false)
	 */
	generateAndStoreSin(phenom, deltaT, amp, offset, difference, overwrite, nbOverwrite) {
		let elapseTime = new Date() - this.initTime;
		let genValue = Number(amp) * Math.sin(2 * Math.PI * elapseTime / 10000) + Number(offset) + Number(difference) * Math.random();
		this.srv.store(phenom, genValue, elapseTime, overwrite, nbOverwrite);
		this.sinLoop = setTimeout(() => {
			// Seul le premier appel doit surcharger
			this.generateAndStoreSin(phenom, deltaT, amp, offset, difference, false, 0)
		}, deltaT);
	}

	/**
	 * Génère et enregistre une valeur "pulse" sur le serveur
	 * @param phenom le nom du phénomène
	 * @param deltaT l'écart de temps entre chaque génération
	 * @param vLow la valeur min
	 * @param t1 le temps 1
	 * @param t2 le temps 2
	 * @param vHigh la valeur max
	 * @param t3 le temps 3
	 * @param t4 le temps 4
	 * @param difference l'écart
	 * @param overwrite true s'il faut écraser des enregistrement
	 * @param nbOverwrite nb d'enregistrement à écraser (0 si overwrite est false)
	 */
	generateAndStorePulse(phenom, deltaT, vLow, t1, t2, vHigh, t3, t4, difference, overwrite, nbOverwrite) {
		//@TODO calculate this value
		let genValue = 0;
		let dateMesure = new Date();

		let elapseTime = (dateMesure - this.initTime) % t4;
		if (elapseTime < t1) {
			genValue = Number(vLow) + Number(difference) * Math.random();
		} else if (elapseTime < t2) {
			genValue = Number(vLow) + (Number(vHigh) - Number(vLow)) / (Number(t2) - Number(t1)) + Number(difference) * Math.random();
		} else if (elapseTime < t3) {
			genValue = Number(vHigh) + Number(difference) * Math.random();
		} else if (elapseTime < t4) {
			genValue = Number(vHigh) + (Number(vLow) - Number(vHigh)) / (Number(t4) - Number(t3)) + Number(difference) * Math.random();
		} else {
			genValue = Number(vLow) + Number(difference) * Math.random();
		}
		this.srv.store(phenom, genValue, overwrite, nbOverwrite);
		this.pulseLoop = setTimeout(() => {
			this.generateAndStorePulse(phenom, deltaT, vLow, t1, t2, vHigh, t3, t4, difference, false, 0)
		}, deltaT);
	}

	/**
	 * Arrête toute génération.
	 */
	stopGeneration() {
		clearTimeout(this.sinLoop);
		clearTimeout(this.pulseLoop);
	}
}
