/**
 * Représentation de la vue de l'application.
 */
export default class View {
	/**
	 * Constructeur de la classe View
	 * @param application
	 */
	constructor(application) {
		this.application = application;
		this.genPhenom = $('#gen-phenom');
		this.overwrite = $('#overwrite');
		this.nbOverwrite = $('#nb-overwrite');
		this.nbOverwriteWrap = $('#nb-overwrite-wrapper');
		this.startGen = $('#start-gen');
		this.execQuery = $('#exec-query');
		this.requestPhenom = $('#request-phenom');
		this.genTable = $('#gen-table');
		this.queryForm = $('#request-form');
		this.genStarted = false;

		/* Config form */
		this.configPhenom = $('#phenom');
		this.configForm = $('#configuration-form');
		this.scaling = $('#scaling');
		this.phenomID = $('#phenom-id');
		this.phenomNature = $('#phenom-nature');
		this.phenomUnit = $('#phenom-unit');

		this.tempGap = $('#temp-gap');

		//Params config
		this.amplitude = $('#amplitude');
		this.offset = $('#offset');
		this.ecartTemp = $('#temp-gap');
		this.vLow = $('#v-low');
		this.vHigh = $('#v-high');
		this.t1 = $('#t1');
		this.t2 = $('#t2');
		this.t3 = $('#t3');
		this.t4 = $('#t4');
		this.ecartLum = $('#ecart-lum');
		this.save = $('#save');
		this.retablir = $('#retablir');
	}

	/**
	 * Initialisation de la vue.
	 */
	initialize() {
		$(document).ready(function () {
			$('select').material_select();
			$('.to-hide').appendTo('#temp-fields');
			$('#phenom').trigger("change");
		});

		this.execQuery.attr('disabled', 'disabled');

		this.execQuery.click((e) => {
			e.preventDefault();
			this.queryForm.submit();
		});

		this.queryForm.submit((e)=> {
			$.ajax({
				type: this.queryForm.attr('method'),
				url: this.queryForm.attr('action'),
				data: this.queryForm.serialize(),
				success: function (response) {
					$('#req-table > tbody').empty();
					$('#req-table > tbody:last-child').append(response);
				},
				error: function (response) {
					console.log(response)
				}
			});

			e.preventDefault();
		});

		this.requestPhenom.change(() => {
			if (this.requestPhenom.val() !== '') {
				this.execQuery.removeAttr('disabled');
			}
		});

		// Afficher/cacher le champ lorsqu'on coche/décoche la case "Ecraser les derniers enregistrements"
		this.overwrite.change(() => {
			if (this.nbOverwriteWrap.hasClass('hide')) {
				this.nbOverwriteWrap.removeClass('hide');
			} else {
				this.nbOverwriteWrap.addClass('hide');
			}
		});

		// Déclaration de l'événement "clique" du bouton start/stop génération
		this.startGen.click((e) => {
			e.preventDefault();
			if (!this.genStarted) {
				let selectedPhenom = this.genPhenom.val();
				let phenomNode = this.application.loadXMLPhenom(selectedPhenom);
				let overwrite = this.overwrite.is(':checked');
				let nbOverwrite = overwrite ? this.nbOverwrite.val() : 0;
				if (overwrite) {
					for (let i = 0; i < nbOverwrite; i++) {
						if (this.genTable.find('tbody > tr:last').length > 0) {
							this.genTable.find('tbody > tr:last').remove();
						}
					}
				}
				// Utilisation d'un au cas où on a d'autres phenomènes à traiter
				switch (selectedPhenom) {
					case 'TXXX':
						this.application.initSin(phenomNode, overwrite, nbOverwrite);
						break;
					case 'LXXX':
						this.application.initPulse(phenomNode);
						break;
					default:
						alert('Unknown phenom');
				}
				this.startGen.text('Stopper la génération');
				this.genStarted = true;
			} else {
				this.application.stopGeneration();
				this.startGen.text('Démarrer la génération');
				this.genStarted = false;
			}
		});

		// Partie visuel config
		this.configPhenom.change(() => {
			let nature = this.configPhenom.find(':selected');
			this.phenomNature.val(nature.attr('data-nature'));
			this.phenomID.val(nature.val());
			this.phenomUnit.val(nature.attr('data-unit'));

			let toDisplay = $('[data-id="' + this.configPhenom.val() + '"]');
			$('.to-hide').appendTo('#temp-fields');
			for (let i = 0; i < toDisplay.length; i++) {
				$(toDisplay[i]).parent().appendTo('#params');
			}
		});

		// Déclaration de l'événement "clique" du bouton Enregistrer (partie config)
		this.save.click((e) => {
			e.preventDefault();
			this.configForm.submit();
		});

		this.configForm.submit((e) => {
			$.ajax({
				type: this.configForm.attr('method'),
				url: this.configForm.attr('action'),
				data: this.configForm.serialize(),
				success: function (response) {
				},
				error: function (response) {
				}
			});

			e.preventDefault();
		});

		// Déclaration de l'événement "clique" du bouton Retablir (partie config)
		this.retablir.click(() => {
			this.amplitude = this.xml.getElementsById('amplitude').getAttribute('value');
			this.offset = this.xml.getElementsById('offset').getAttribute('value');
			this.ecartTemp = this.xml.getElementsById('ecartTEMP').getAttribute('value');
			this.vLow = this.xml.getElementsById('VLOW').getAttribute('value');
			this.vHigh = this.xml.getElementsById('T1').getAttribute('value');
			this.t1 = this.xml.getElementsById('T2').getAttribute('value');
			this.t2 = this.xml.getElementsById('VHIGH').getAttribute('value');
			this.t3 = this.xml.getElementsById('T3').getAttribute('value');
			this.t4 = this.xml.getElementsById('T4').getAttribute('value');
			this.ecartLum = this.xml.getElementsById('ecartLUM').getAttribute('value');
		});
	}
}
