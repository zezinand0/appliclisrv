import View from "./View";
import Generator from "./Generator";

export default class Application {

	/**
	 * Constructor of the Application
	 */
	constructor() {
		this.generator = new Generator();
		this.view = new View(this);
		this.nodeList = null;
		this.xml = Application.loadByBrowser("configuration.xml");
	}

	start() {
		this.view.initialize();
	}

	stopGeneration() {
		this.generator.stopGeneration();
	}

	setXML() {
		this.xml = Application.loadByBrowser("configuration.xml");
	}

	loadXMLPhenom(selectedPhenom) {
		this.nodeList = this.xml.getElementsByTagName('phenomene');
		this.generator.setInitTime(new Date());
		let phenom = null;
		for (let i = 0; i < this.nodeList.length; i++) {
			let element = this.nodeList[i];
			if(element.getAttribute('id') === selectedPhenom) {
				phenom = element;
			}
		}

		return phenom
	}

	static loadByBrowser(fileName) {
		let xmlDoc;
		try {
			let xmlhttp = new XMLHttpRequest();
			xmlhttp.open('GET', fileName, false);
			xmlhttp.setRequestHeader('Content-Type', 'text/xml');
			xmlhttp.send('');
			xmlDoc = xmlhttp.responseXML;
		} catch (ex) {
			try {
				// xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
			} catch (e) {
				alert(e.message);
			}
		}

		return xmlDoc;
	}

	initSin(phenomNode, overwrite, nbOverwrite) {
		let amp = this.xml.getElementById("amplitude").getAttribute("valeur");
		let offset = this.xml.getElementById("offset").getAttribute("valeur");
		let difference = this.xml.getElementById("ecartTEMP").getAttribute("valeur");
		let deltaT = phenomNode.getAttribute("sampling");
		let phenom = phenomNode.getAttribute("name");
		this.generator.generateAndStoreSin(phenom, deltaT, amp, offset, difference, overwrite, nbOverwrite);
	}

	initPulse(phenomNode, overwrite, nbOverwrite) {
		let vLow = this.xml.getElementById("VLOW").getAttribute("valeur");
		let vHigh = this.xml.getElementById("VHIGH").getAttribute("valeur");
		let difference = this.xml.getElementById("ecartLUM").getAttribute("valeur");
		let t1 = this.xml.getElementById("T1").getAttribute("valeur");
		let t2 = this.xml.getElementById("T2").getAttribute("valeur");
		let t3 = this.xml.getElementById("T3").getAttribute("valeur");
		let t4 = this.xml.getElementById("T4").getAttribute("valeur");
		let deltaT = phenomNode.getAttribute("sampling");
		let phenom = phenomNode.getAttribute("name");
		console.log(phenom, deltaT, vLow, t1, t2, vHigh, t3, t4, difference, overwrite, nbOverwrite);
		this.generator.generateAndStorePulse(phenom, deltaT, vLow, t1, t2, vHigh, t3, t4, difference, overwrite, nbOverwrite);
	}
}
