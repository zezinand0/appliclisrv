(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _View = require("./View");

var _View2 = _interopRequireDefault(_View);

var _Generator = require("./Generator");

var _Generator2 = _interopRequireDefault(_Generator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Application = function () {

	/**
  * Constructor of the Application
  */
	function Application() {
		_classCallCheck(this, Application);

		this.generator = new _Generator2.default();
		this.view = new _View2.default(this);
		this.nodeList = null;
		this.xml = Application.loadByBrowser("configuration.xml");
	}

	_createClass(Application, [{
		key: "start",
		value: function start() {
			this.view.initialize();
		}
	}, {
		key: "stopGeneration",
		value: function stopGeneration() {
			this.generator.stopGeneration();
		}
	}, {
		key: "setXML",
		value: function setXML() {
			this.xml = Application.loadByBrowser("configuration.xml");
		}
	}, {
		key: "loadXMLPhenom",
		value: function loadXMLPhenom(selectedPhenom) {
			this.nodeList = this.xml.getElementsByTagName('phenomene');
			this.generator.setInitTime(new Date());
			var phenom = null;
			for (var i = 0; i < this.nodeList.length; i++) {
				var element = this.nodeList[i];
				if (element.getAttribute('id') === selectedPhenom) {
					phenom = element;
				}
			}

			return phenom;
		}
	}, {
		key: "initSin",
		value: function initSin(phenomNode, overwrite, nbOverwrite) {
			var amp = this.xml.getElementById("amplitude").getAttribute("valeur");
			var offset = this.xml.getElementById("offset").getAttribute("valeur");
			var difference = this.xml.getElementById("ecartTEMP").getAttribute("valeur");
			var deltaT = phenomNode.getAttribute("sampling");
			var phenom = phenomNode.getAttribute("name");
			this.generator.generateAndStoreSin(phenom, deltaT, amp, offset, difference, overwrite, nbOverwrite);
		}
	}, {
		key: "initPulse",
		value: function initPulse(phenomNode, overwrite, nbOverwrite) {
			var vLow = this.xml.getElementById("VLOW").getAttribute("valeur");
			var vHigh = this.xml.getElementById("VHIGH").getAttribute("valeur");
			var difference = this.xml.getElementById("ecartLUM").getAttribute("valeur");
			var t1 = this.xml.getElementById("T1").getAttribute("valeur");
			var t2 = this.xml.getElementById("T2").getAttribute("valeur");
			var t3 = this.xml.getElementById("T3").getAttribute("valeur");
			var t4 = this.xml.getElementById("T4").getAttribute("valeur");
			var deltaT = phenomNode.getAttribute("sampling");
			var phenom = phenomNode.getAttribute("name");
			console.log(phenom, deltaT, vLow, t1, t2, vHigh, t3, t4, difference, overwrite, nbOverwrite);
			this.generator.generateAndStorePulse(phenom, deltaT, vLow, t1, t2, vHigh, t3, t4, difference, overwrite, nbOverwrite);
		}
	}], [{
		key: "loadByBrowser",
		value: function loadByBrowser(fileName) {
			var xmlDoc = void 0;
			try {
				var xmlhttp = new XMLHttpRequest();
				xmlhttp.open('GET', fileName, false);
				xmlhttp.setRequestHeader('Content-Type', 'text/xml');
				xmlhttp.send('');
				xmlDoc = xmlhttp.responseXML;
			} catch (ex) {
				try {
					// xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
				} catch (e) {
					alert(e.message);
				}
			}

			return xmlDoc;
		}
	}]);

	return Application;
}();

exports.default = Application;

},{"./Generator":2,"./View":5}],2:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Server = require("./Server");

var _Server2 = _interopRequireDefault(_Server);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Generator = function () {

	/**
  * Constructor of the Generator
  */
	function Generator() {
		_classCallCheck(this, Generator);

		this.initTime = 0;
		this.sinLoop = null;
		this.pulseLoop = null;
		this.srv = new _Server2.default();
	}

	/**
  * Méthode pour attribuer une valeur à l'attribut "initTime"
  * @param value
  */


	_createClass(Generator, [{
		key: "setInitTime",
		value: function setInitTime(value) {
			this.initTime = value;
		}

		/**
   * Génère et enregistre une valeur "sin" sur le serveur.
   * @param phenom le nom du phénomène
   * @param deltaT l'écart de temps entre chaque génération
   * @param amp l'amplitude
   * @param offset le décallage
   * @param difference l'écart
   * @param overwrite true s'il faut écraser des enregistrement
   * @param nbOverwrite nb d'enregistrement à écraser (0 si overwrite est false)
   */

	}, {
		key: "generateAndStoreSin",
		value: function generateAndStoreSin(phenom, deltaT, amp, offset, difference, overwrite, nbOverwrite) {
			var _this = this;

			var elapseTime = new Date() - this.initTime;
			var genValue = Number(amp) * Math.sin(2 * Math.PI * elapseTime / 10000) + Number(offset) + Number(difference) * Math.random();
			this.srv.store(phenom, genValue, elapseTime, overwrite, nbOverwrite);
			this.sinLoop = setTimeout(function () {
				// Seul le premier appel doit surcharger
				_this.generateAndStoreSin(phenom, deltaT, amp, offset, difference, false, 0);
			}, deltaT);
		}

		/**
   * Génère et enregistre une valeur "pulse" sur le serveur
   * @param phenom le nom du phénomène
   * @param deltaT l'écart de temps entre chaque génération
   * @param vLow la valeur min
   * @param t1 le temps 1
   * @param t2 le temps 2
   * @param vHigh la valeur max
   * @param t3 le temps 3
   * @param t4 le temps 4
   * @param difference l'écart
   * @param overwrite true s'il faut écraser des enregistrement
   * @param nbOverwrite nb d'enregistrement à écraser (0 si overwrite est false)
   */

	}, {
		key: "generateAndStorePulse",
		value: function generateAndStorePulse(phenom, deltaT, vLow, t1, t2, vHigh, t3, t4, difference, overwrite, nbOverwrite) {
			var _this2 = this;

			//@TODO calculate this value
			var genValue = 0;
			var dateMesure = new Date();

			var elapseTime = (dateMesure - this.initTime) % t4;
			if (elapseTime < t1) {
				genValue = Number(vLow) + Number(difference) * Math.random();
			} else if (elapseTime < t2) {
				genValue = Number(vLow) + (Number(vHigh) - Number(vLow)) / (Number(t2) - Number(t1)) + Number(difference) * Math.random();
			} else if (elapseTime < t3) {
				genValue = Number(vHigh) + Number(difference) * Math.random();
			} else if (elapseTime < t4) {
				genValue = Number(vHigh) + (Number(vLow) - Number(vHigh)) / (Number(t4) - Number(t3)) + Number(difference) * Math.random();
			} else {
				genValue = Number(vLow) + Number(difference) * Math.random();
			}
			this.srv.store(phenom, genValue, overwrite, nbOverwrite);
			this.pulseLoop = setTimeout(function () {
				_this2.generateAndStorePulse(phenom, deltaT, vLow, t1, t2, vHigh, t3, t4, difference, false, 0);
			}, deltaT);
		}

		/**
   * Arrête toute génération.
   */

	}, {
		key: "stopGeneration",
		value: function stopGeneration() {
			clearTimeout(this.sinLoop);
			clearTimeout(this.pulseLoop);
		}
	}]);

	return Generator;
}();

exports.default = Generator;

},{"./Server":4}],3:[function(require,module,exports){
"use strict";

var _Application = require("./Application");

var _Application2 = _interopRequireDefault(_Application);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = new _Application2.default();
app.start();

},{"./Application":1}],4:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Server = function () {

	/**
  * Constructor of the Server
  */
	function Server() {
		_classCallCheck(this, Server);

		this.genTable = $('#gen-table > tbody:last-child');
		this.reqTable = $('#req-table > tbody:last-child');
		this.getMeasures();
	}

	_createClass(Server, [{
		key: 'getMeasures',
		value: function getMeasures() {
			var _this = this;

			$.ajax({
				type: 'POST',
				url: 'server',
				data: {
					'key': 'xdjosia'
				},
				success: function success(response) {
					_this.genTable.append(response);
					_this.reqTable.append(response);
				},
				error: function error(response) {
					console.log(response);
				}
			});
		}
	}, {
		key: 'store',
		value: function store(phenom, value, genTime, overwrite, nbOverwrite) {
			var _this2 = this;

			$.ajax({
				type: 'POST',
				url: 'server/insert',
				data: {
					'phenom': phenom,
					'value': value,
					'genTime': genTime,
					'overwrite': overwrite,
					'nbOverwrite': nbOverwrite
				},
				success: function success(response) {
					_this2.genTable.append(response);
					_this2.reqTable.append(response);
				},
				error: function error(response) {
					console.log(response);
				}
			});
		}
	}]);

	return Server;
}();

exports.default = Server;

},{}],5:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Représentation de la vue de l'application.
 */
var View = function () {
	/**
  * Constructeur de la classe View
  * @param application
  */
	function View(application) {
		_classCallCheck(this, View);

		this.application = application;
		this.genPhenom = $('#gen-phenom');
		this.overwrite = $('#overwrite');
		this.nbOverwrite = $('#nb-overwrite');
		this.nbOverwriteWrap = $('#nb-overwrite-wrapper');
		this.startGen = $('#start-gen');
		this.execQuery = $('#exec-query');
		this.requestPhenom = $('#request-phenom');
		this.genTable = $('#gen-table');
		this.queryForm = $('#request-form');
		this.genStarted = false;

		/* Config form */
		this.configPhenom = $('#phenom');
		this.configForm = $('#configuration-form');
		this.scaling = $('#scaling');
		this.phenomID = $('#phenom-id');
		this.phenomNature = $('#phenom-nature');
		this.phenomUnit = $('#phenom-unit');

		this.tempGap = $('#temp-gap');

		//Params config
		this.amplitude = $('#amplitude');
		this.offset = $('#offset');
		this.ecartTemp = $('#temp-gap');
		this.vLow = $('#v-low');
		this.vHigh = $('#v-high');
		this.t1 = $('#t1');
		this.t2 = $('#t2');
		this.t3 = $('#t3');
		this.t4 = $('#t4');
		this.ecartLum = $('#ecart-lum');
		this.save = $('#save');
		this.retablir = $('#retablir');
	}

	/**
  * Initialisation de la vue.
  */


	_createClass(View, [{
		key: 'initialize',
		value: function initialize() {
			var _this = this;

			$(document).ready(function () {
				$('select').material_select();
				$('.to-hide').appendTo('#temp-fields');
				$('#phenom').trigger("change");
			});

			this.execQuery.attr('disabled', 'disabled');

			this.execQuery.click(function (e) {
				e.preventDefault();
				_this.queryForm.submit();
			});

			this.queryForm.submit(function (e) {
				$.ajax({
					type: _this.queryForm.attr('method'),
					url: _this.queryForm.attr('action'),
					data: _this.queryForm.serialize(),
					success: function success(response) {
						$('#req-table > tbody').empty();
						$('#req-table > tbody:last-child').append(response);
					},
					error: function error(response) {
						console.log(response);
					}
				});

				e.preventDefault();
			});

			this.requestPhenom.change(function () {
				if (_this.requestPhenom.val() !== '') {
					_this.execQuery.removeAttr('disabled');
				}
			});

			// Afficher/cacher le champ lorsqu'on coche/décoche la case "Ecraser les derniers enregistrements"
			this.overwrite.change(function () {
				if (_this.nbOverwriteWrap.hasClass('hide')) {
					_this.nbOverwriteWrap.removeClass('hide');
				} else {
					_this.nbOverwriteWrap.addClass('hide');
				}
			});

			// Déclaration de l'événement "clique" du bouton start/stop génération
			this.startGen.click(function (e) {
				e.preventDefault();
				if (!_this.genStarted) {
					var selectedPhenom = _this.genPhenom.val();
					var phenomNode = _this.application.loadXMLPhenom(selectedPhenom);
					var overwrite = _this.overwrite.is(':checked');
					var nbOverwrite = overwrite ? _this.nbOverwrite.val() : 0;
					if (overwrite) {
						for (var i = 0; i < nbOverwrite; i++) {
							if (_this.genTable.find('tbody > tr:last').length > 0) {
								_this.genTable.find('tbody > tr:last').remove();
							}
						}
					}
					// Utilisation d'un au cas où on a d'autres phenomènes à traiter
					switch (selectedPhenom) {
						case 'TXXX':
							_this.application.initSin(phenomNode, overwrite, nbOverwrite);
							break;
						case 'LXXX':
							_this.application.initPulse(phenomNode);
							break;
						default:
							alert('Unknown phenom');
					}
					_this.startGen.text('Stopper la génération');
					_this.genStarted = true;
				} else {
					_this.application.stopGeneration();
					_this.startGen.text('Démarrer la génération');
					_this.genStarted = false;
				}
			});

			// Partie visuel config
			this.configPhenom.change(function () {
				var nature = _this.configPhenom.find(':selected');
				_this.phenomNature.val(nature.attr('data-nature'));
				_this.phenomID.val(nature.val());
				_this.phenomUnit.val(nature.attr('data-unit'));

				var toDisplay = $('[data-id="' + _this.configPhenom.val() + '"]');
				$('.to-hide').appendTo('#temp-fields');
				for (var i = 0; i < toDisplay.length; i++) {
					$(toDisplay[i]).parent().appendTo('#params');
				}
			});

			// Déclaration de l'événement "clique" du bouton Enregistrer (partie config)
			this.save.click(function (e) {
				e.preventDefault();
				_this.configForm.submit();
			});

			this.configForm.submit(function (e) {
				$.ajax({
					type: _this.configForm.attr('method'),
					url: _this.configForm.attr('action'),
					data: _this.configForm.serialize(),
					success: function success(response) {},
					error: function error(response) {}
				});

				e.preventDefault();
			});

			// Déclaration de l'événement "clique" du bouton Retablir (partie config)
			this.retablir.click(function () {
				_this.amplitude = _this.xml.getElementsById('amplitude').getAttribute('value');
				_this.offset = _this.xml.getElementsById('offset').getAttribute('value');
				_this.ecartTemp = _this.xml.getElementsById('ecartTEMP').getAttribute('value');
				_this.vLow = _this.xml.getElementsById('VLOW').getAttribute('value');
				_this.vHigh = _this.xml.getElementsById('T1').getAttribute('value');
				_this.t1 = _this.xml.getElementsById('T2').getAttribute('value');
				_this.t2 = _this.xml.getElementsById('VHIGH').getAttribute('value');
				_this.t3 = _this.xml.getElementsById('T3').getAttribute('value');
				_this.t4 = _this.xml.getElementsById('T4').getAttribute('value');
				_this.ecartLum = _this.xml.getElementsById('ecartLUM').getAttribute('value');
			});
		}
	}]);

	return View;
}();

exports.default = View;

},{}]},{},[3]);
