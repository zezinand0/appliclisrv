<?php
namespace HEIG\Domo\Utility;

/**
 * Classe de configuration
 * @package HEIG\Domo\Utility
 */
class Configuration
{
    /** Configuration de la connexion à la base de données */
    const DB_HOSTNAME = 'jeremie-roulin.local';
    const DB_USER = 'root';
    const DB_PWD = 'root';
    const DB_NAME = 'Mesures';

    /** Liste des urls disponnibles */
    const ROUTES = [
        [
            'path' => '',
            'action' => 'index',
            'settings' => []
        ],
        [
            'path' => 'insert',
            'action' => 'insert',
            'settings' => []
        ],
        [
            'path' => 'request',
            'action' => 'request',
            'settings' => []
        ],
        [
            'path' => 'configure',
            'action' => 'configure',
            'settings' => []
        ],
        [
            'path' => 'exec-query',
            'action' => 'execQuery',
            'settings' => []
        ]
    ];
}