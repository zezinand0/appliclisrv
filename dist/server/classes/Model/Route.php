<?php
namespace HEIG\Domo\Model;

/**
 * Class Route
 * @package HEIG\Domo\Model
 */
class Route
{
    /**
     * @var string
     */
    protected $path = '';

    /**
     * @var string
     */
    protected $action = '';

    /**
     * @var array
     */
    protected $settings = [];

    /**
     * The constructor.
     *
     * @param string $path
     * @param string $action
     * @param array $settings
     */
    public function __construct($path, $action, array $settings = [])
    {
        $this->path = $path;
        $this->action = $action . 'Action';
        $this->settings = $settings;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }
}