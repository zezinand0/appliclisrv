<?php
namespace HEIG\Domo\Controller;

use DOMDocument;
use DOMXPath;
use HEIG\Domo\DB\MysqliDb;
use HEIG\Domo\Utility\Configuration;

/**
 * Class Controller
 * @package HEIG\Domo\Controller
 */
class Controller
{

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->db = new MysqliDb (
            Configuration::DB_HOSTNAME,
            Configuration::DB_USER,
            Configuration::DB_PWD,
            Configuration::DB_NAME
        );
    }

    /**
     * Sélectionne et affiche toutes les
     */
    public function indexAction()
    {
        $measures = $this->db->get('Mesures');
        if ($measures) {
            foreach ($measures as $row) {
                echo "<tr><td>" . $row['Phenom'] . "</td><td>" . $row['Valeur'] . "</td><td>" . $row['Instant'] . "</td></tr>";
            }
        } else {
            echo 'failed to load data: ' . $this->db->getLastError();
        }
    }

    public function insertAction()
    {
        if ($_POST['overwrite']) {
            $this->db->delete('Mesures', (int)$_POST['nbOverwrite']);
        }
        $data = [
            'Phenom' => $_POST['phenom'],
            'Valeur' => $_POST['value'],
            'Instant' => date('Y-m-d H:i:s')
        ];
        if ($this->db->insert('Mesures', $data)) {
            echo "<tr><td>" . $data['Phenom'] . "</td><td>" . $data['Valeur'] . "</td><td>" . $data['Instant'] . "</td></tr>";
        } else {
            echo 'insert failed: ' . $this->db->getLastError();
        };
    }

    public function execQueryAction()
    {
        $this->db->where('Phenom', $_POST['request-phenom']);
        $this->db->orderBy($_POST['request-sortby'], $_POST['request-order']);

        if (!empty($_POST['request-limit'])) {
            $measures = $this->db->get('Mesures', $_POST['request-limit']);
        } else {
            $measures = $this->db->get('Mesures');
        }

        if ($measures) {
            foreach ($measures as $measure) {
                echo "<tr><td>" . $measure['Phenom'] . "</td><td>" . $measure['Valeur'] . "</td><td>" . $measure['Instant'] . "</td></tr>";
            }
        } else {
            echo 'select failed: ' . $this->db->getLastError();
        }
    }

    public function configureAction()
    {
        $doc = new DOMDocument;
        $doc->load('../configuration.xml');

        $xp = new DomXPath($doc);

        foreach ($_POST as $key => $param) {
            $res = $xp->query("//param[@id='" . $key . "']");
            if ($res->item(0)) {
                $res->item(0)->setAttribute('valeur', $param);
            }
        }

        $doc->save('../configuration.xml');
    }
}