<?php
namespace HEIG\Domo\Routing;

use HEIG\Domo\Controller\Controller;
use HEIG\Domo\Model\Route;

/**
 * Classe qui redirige vers l'action en fonction du paramètre d'url reçu.
 * @package HEIG\Domo\Routing
 */
class Dispatcher
{
    /**
     * @var Controller
     */
    protected $controller;

    /**
     * Dispatcher constructor.
     */
    public function __construct()
    {
        $this->controller = new Controller();
    }

    /**
     * Dispatches the request.
     */
    public function dispatch()
    {
        $requestedRoute = (new Router())->getRequestedRoute();

        if ($requestedRoute === null) {
            $requestedRoute = $this->getPageNotFound();
        }

        $this->controller->{$requestedRoute->getAction()}();
    }

    /**
     * Returns a route for a page not found.
     *
     * @return Route
     */
    protected function getPageNotFound()
    {
        header('Location: /erreur-404', false, 404);
        return (new Route('erreur-404', 'Erreur 404', 'index'));
    }
}