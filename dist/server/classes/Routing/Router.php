<?php
namespace HEIG\Domo\Routing;


use HEIG\Domo\Model\Route;
use HEIG\Domo\Utility\Configuration;

/**
 * Class Router
 * @package HEIG\Domo\Routing
 */
class Router
{
    /**
     * @var array
     */
    protected $registeredRoutes = [];

    /**
     * The constructor stores the registered routes in an array.
     */
    public function __construct()
    {
        foreach (Configuration::ROUTES as $route) {
            $this->registeredRoutes[] = (new Route(
                $route['path'] === null ? '' : $route['path'],
                $route['action'] === null ? 'index' : $route['action'],
                $route['settings'] === null ? [] : $route['settings']
            ));
        }
    }

    /**
     * Returns the requested route.
     *
     * @return null|Route
     */
    public function getRequestedRoute()
    {
        $requestedRoute = null;

        foreach ($this->registeredRoutes as $registeredRoute) {
            $requestUriPart = explode('/', $_SERVER['REQUEST_URI']);
            if ($registeredRoute->getPath() === end($requestUriPart)) {
                $requestedRoute = $registeredRoute;
                break;
            }
        }

        return $requestedRoute;
    }
}