<?php

call_user_func(function() {
    require __DIR__ . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

    (new \HEIG\Domo\Routing\Dispatcher())->dispatch();
});