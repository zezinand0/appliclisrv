<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
    <xsl:key name="phenomById" match="phenom" use="@id"/>
    <xsl:key name="classById" match="class" use="@id"/>
    <xsl:output method="html" doctype-system="about:legacy-compat" encoding="utf-8" indent="yes"/>
    <xsl:template match="/">

        <html lang="fr">
            <head>
                <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
                <link type="text/css" rel="stylesheet" href="css/styles.min.css" media="screen,projection"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <meta charset="utf-8"/>
            </head>
            <body>
                <div class="container">
                    <xsl:apply-templates select="//phenomenes"/>
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="phenomenes">
        <div class="row">
            <div class="col m8 s12">
                <ul class="tabs">
                    <li class="tab col s3">
                        <a class="active" href="#generation-tab">Génération</a>
                    </li>
                    <li class="tab col s3">
                        <a href="#request-tab">Requêtes</a>
                    </li>
                    <li class="tab col s3">
                        <a href="#configuration-tab">Configuration</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div id="generation-tab">
                <form id="generation-form" action="#">
                    <div class="input-field col s12 m3">
                        <select id="gen-phenom">
                            <xsl:for-each select="./phenomene">
                                <xsl:element name="option">
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="./@id"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="data-nature">
                                        <xsl:value-of select="./@nature"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="data-unit">
                                        <xsl:value-of select="./@unit"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="data-sampling">
                                        <xsl:value-of select="./@sampling"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="data-max-sampling">
                                        <xsl:value-of select="./@max-sampling"/>
                                    </xsl:attribute>
                                    <xsl:if test="position()=1">
                                        <xsl:attribute name="selected">selected</xsl:attribute>
                                    </xsl:if>
                                    <xsl:value-of select="./@name"/>
                                </xsl:element>
                            </xsl:for-each>
                        </select>
                        <label for="gen-phenom">Phénomène</label>
                    </div>
                    <div class="clearfix"></div>
                    <p class="col s12 m3">
                        <xsl:element name="input">
                            <xsl:attribute name="type">checkbox</xsl:attribute>
                            <xsl:attribute name="id">overwrite</xsl:attribute>
                        </xsl:element>
                        <label for="overwrite">Ecraser les dernières mesures</label>
                    </p>
                    <div class="clearfix"></div>
                    <div id="nb-overwrite-wrapper" class="col s4 m3 hide">
                        <label class="active" for="nb-overwrite">Nombre de mesures à écraser</label>
                        <xsl:element name="input">
                            <xsl:attribute name="value">10</xsl:attribute>
                            <xsl:attribute name="min">0</xsl:attribute>
                            <xsl:attribute name="id">nb-overwrite</xsl:attribute>
                            <xsl:attribute name="type">number</xsl:attribute>
                        </xsl:element>
                    </div>
                </form>
                <div class="col s12">
                    <p>
                        <a id="start-gen" class="waves-effect waves-light btn"><i class="material-icons left">send</i>Démarrer la
                            génération
                        </a>
                    </p>
                    <table id="gen-table" class="striped">
                        <thead>
                            <tr>
                                <th data-field="phenom">Phénomène</th>
                                <th data-field="value">Valeur</th>
                                <th data-field="time">Instant</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="request-tab">
                <form id="request-form" action="server/exec-query" method="post">
                    <div class="input-field col s12 m3">
                        <select id="request-phenom" name="request-phenom">
                            <option value="" disabled="disabled" selected="selected">Quel phénomène afficher?</option>
                            <xsl:for-each select="./phenomene">
                                <xsl:element name="option">
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="./@name"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="data-nature">
                                        <xsl:value-of select="./@nature"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="data-unit">
                                        <xsl:value-of select="./@unit"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="data-sampling">
                                        <xsl:value-of select="./@sampling"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="data-max-sampling">
                                        <xsl:value-of select="./@max-sampling"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="./@name"/>
                                </xsl:element>
                            </xsl:for-each>
                        </select>
                        <label for="request-phenom">Phénomène</label>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col s4 m3">
                        <label class="active" for="request-limit">Limite</label>
                        <input min="0" id="request-limit" name="request-limit" type="number"/>
                    </div>
                    <div class="clearfix"></div>
                    <div class="input-field col s12 m3">
                        <select id="request-sortby" name="request-sortby">
                            <option value="Instant" selected="selected">Date</option>
                            <option value="Valeur">Valeur</option>
                        </select>
                        <label for="request-phenom">Trier par</label>
                    </div>
                    <div class="input-field col s12 m3">
                        <select id="request-order" name="request-order">
                            <option value="ASC" selected="selected">Ascendant</option>
                            <option value="DESC">Descendant</option>
                        </select>
                        <label for="request-phenom">Ordre</label>
                    </div>
                </form>
                <div class="col s12">
                    <p>
                        <a id="exec-query" class="waves-effect waves-light btn"><i class="material-icons left">done</i>Exécuter la
                            requête
                        </a>
                    </p>
                    <table id="req-table" class="striped">
                        <thead>
                            <tr>
                                <th data-field="phenom">Phénomène</th>
                                <th data-field="value">Valeur</th>
                                <th data-field="time">Instant</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <div id="configuration-tab">
                <form id="configuration-form" method="post" action="server/configure">
                    <div class="input-field col s12 m3">
                        <select id="phenom" name="phenom">
                            <xsl:for-each select="./phenomene">
                                <xsl:element name="option">
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="./@id"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="data-nature">
                                        <xsl:value-of select="./@nature"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="data-unit">
                                        <xsl:value-of select="./@unit"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="data-sampling">
                                        <xsl:value-of select="./@sampling"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="data-max-sampling">
                                        <xsl:value-of select="./@max-sampling"/>
                                    </xsl:attribute>
                                    <xsl:if test="position()=1">
                                        <xsl:attribute name="selected">selected</xsl:attribute>
                                    </xsl:if>
                                    <xsl:value-of select="./@name"/>
                                </xsl:element>
                            </xsl:for-each>
                        </select>
                        <label for="phenom">Phénomène</label>
                    </div>
                    <div class="clearfix"></div>
                    <p class="range-field col s12 m3">
                        <label for="scaling">Echantillonage</label>
                        <xsl:for-each select="./phenomene">
                            <xsl:if test="position()=1">
                                <xsl:element name="input">
                                    <xsl:attribute name="type">range</xsl:attribute>
                                    <xsl:attribute name="id">scaling</xsl:attribute>
                                    <xsl:attribute name="name">scaling</xsl:attribute>
                                    <xsl:attribute name="min">0</xsl:attribute>
                                    <xsl:attribute name="max">
                                        <xsl:value-of select="./@max-sampling"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="./@sampling"/>
                                    </xsl:attribute>
                                </xsl:element>
                            </xsl:if>
                        </xsl:for-each>

                    </p>
                    <div class="clearfix"></div>
                    <div class="input-field col s4">
                        <xsl:for-each select="./phenomene">
                            <xsl:if test="position()=1">
                                <xsl:element name="input">
                                    <xsl:attribute name="type">text</xsl:attribute>
                                    <xsl:attribute name="id">phenom-id</xsl:attribute>
                                    <xsl:attribute name="name">phenom-id</xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="./@id"/>
                                    </xsl:attribute>
                                </xsl:element>
                            </xsl:if>
                        </xsl:for-each>
                        <label for="phenom-id">ID</label>
                    </div>
                    <div class="input-field col s4">
                        <xsl:for-each select="./phenomene">
                            <xsl:if test="position()=1">
                                <xsl:element name="input">
                                    <xsl:attribute name="type">text</xsl:attribute>
                                    <xsl:attribute name="id">phenom-nature</xsl:attribute>
                                    <xsl:attribute name="name">phenom-nature</xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="./@nature"/>
                                    </xsl:attribute>
                                </xsl:element>
                            </xsl:if>
                        </xsl:for-each>
                        <label for="phenom-nature">Nature</label>
                    </div>
                    <div class="input-field col s4">
                        <xsl:for-each select="./phenomene">
                            <xsl:if test="position()=1">
                                <xsl:element name="input">
                                    <xsl:attribute name="type">text</xsl:attribute>
                                    <xsl:attribute name="id">phenom-unit</xsl:attribute>
                                    <xsl:attribute name="name">phenom-unit</xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="./@unit"/>
                                    </xsl:attribute>
                                </xsl:element>
                            </xsl:if>
                        </xsl:for-each>
                        <label for="phenom-unit">Unité</label>
                    </div>
                    <fieldset id="params">
                        <legend>Paramètres</legend>
                        <xsl:for-each select="./phenomene/param">
                            <p class="range-field col s12 m4 to-hide">
                                <xsl:element name="label">
                                    <xsl:attribute name="for">
                                        <xsl:value-of select="./@id"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="data-id">
                                        <xsl:value-of select="../@id"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="./@id"/>
                                </xsl:element>
                                <xsl:element name="input">
                                    <xsl:attribute name="id">
                                        <xsl:value-of select="./@id"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="name">
                                        <xsl:value-of select="./@id"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="data-id">
                                        <xsl:value-of select="../@id"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="type">range</xsl:attribute>
                                    <xsl:attribute name="min">
                                        <xsl:value-of select="./@min"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="max">
                                        <xsl:value-of select="./@max"/>
                                    </xsl:attribute>
                                    <xsl:attribute name="value">
                                        <xsl:value-of select="./@valeur"/>
                                    </xsl:attribute>
                                </xsl:element>
                            </p>
                        </xsl:for-each>
                    </fieldset>
                </form>
                <p>
                    <a id="save" class="waves-effect waves-light btn">
                        <i class="material-icons left">done</i>
                        Enregistrer
                    </a>
                    <a id="retablir" class="waves-effect waves-light btn disabled">
                        <i class="material-icons left">fast_rewind</i>
                        Rétablir
                    </a>
                </p>
                <div id="temp-fields" class="hide"></div>
            </div>
        </div>
        <script type="text/javascript" src="vendors/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="vendors/materialize/dist/js/materialize.min.js"></script>
        <script type="text/javascript" src="js/Main.js"></script>
    </xsl:template>
</xsl:stylesheet>